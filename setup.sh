#!/bin/sh
# mjxeta's bootstrap script v1.4.0
#------------------------------ VARIABLES ------------------------------#
PASSDISKCONFIRM=password
CHOOSENDISK_CONFIRM=X

#------------------------------ FUNCTIONS ------------------------------#

choosedisk() {
DISKS=$(lsblk --noheadings -dro NAME | awk -vORS=" " '1')
IFS=' ' read -ra DISKLABEL <<< $DISKS
while [ -z $CHOOSENDISK ] && [ $CHOOSENDISK_CONFIRM = X ] ; do
	CHOOSENDISK=$(dialog --no-cancel --menu 'Choose disk to format' 15 60 1 \
		"${DISKLABEL[0]}" '' "${DISKLABEL[1]}" '' \
		"${DISKLABEL[2]}" '' "${DISKLABEL[3]}" '' \
		"${DISKLABEL[4]}" '' "${DISKLABEL[5]}" ''  3>&1 1>&2 2>&3 3>&1)
	[ -z $CHOOSENDISK ] && continue
	(DIALOGRC=$(pwd)/.dialogrc dialog --colors --yesno \
	"\Zb\Z0This will destroy all data on that drive.\n
	Please double check before well continue" \
	8 60 3>&1 1>&2 2>&3 3>&1) || exit 1
done
}

initmsg() {
	dialog --colors --title "\Z0Hello!" \
	--yes-label "Let's go" --no-label "Leave me alone!" --yesno \
	"Thanks for trying my script ...
	If you find any bugs, or something that can be done better,
	please contact me on gitlab.
	My gpg key and email will be avaible soon ..." 12 60 || \
		exit 1
}

passworddisk() {
	PASSDISK=$(DIALOGRC=$(pwd)/.greydialogrc dialog --no-cancel --passwordbox \
		"Enter password for encrypted partition" 8 45 3>&1 1>&2 2>&3 3>&1)
	PASSDISKCONFIRM=$(DIALOGRC=$(pwd)/.greydialogrc dialog --no-cancel --passwordbox \
		"Confirm password" 8 45 3>&1 1>&2 2>&3 3>&1)
	until [ $PASSDISK = $PASSDISKCONFIRM ] && [ $PASSDISK != password ] ; do
		PASSDISK=$(DIALOGRC=$(pwd)/.greydialogrc dialog --colors --no-cancel \
			--passwordbox "Passwords doesn't match	\Z11" 8 45 3>&1 1>&2 2>&3 3>&1)
		PASSDISKCONFIRM=$(DIALOGRC=$(pwd)/.greydialogrc dialog --colors --no-cancel \
			--passwordbox "Confirm password again	\Z12" 8 45 3>&1 1>&2 2>&3 3>&1)
	done
}

netcheck() {
	ping -q -c1 x.org >/dev/null 2>/dev/null || {
		DIALOGRC=$(pwd)/.dialogrc dialog --colors \
		--msgbox "\Zb\Z1\ZuNo internet connection !" 5 28  && exit 1
	}
}

#------------------------------ SCRIPT BASE -----------------------------#

initmsg
netcheck
choosedisk
# update clock via ntp
#timedatectl set-ntp true

#------------------------------ SCRATCHPAD ------------------------------#

TYPEINSTALL=$(dialog --no-cancel --menu 'Choose type of installation:' \
	12 30 3 short '' advanced '' manual '' 3>&1 1>&2 2>&3 3>&1)
case $TYPEINSTALL in
		short)	dialog --yesno "Encrypt the drive?" 7 30 && CRYPTO=0 ;;
     advanced);;
       manual);;
esac

[ $CRYPTO ] && passworddisk

FILESYS=$(dialog --no-cancel --radiolist 'title' 15 60 10 'ext4' 'recommended'\
	'on' 'xfs' '' 'off' 'btrfs' '' 'off' 3>&1 1>&2 2>&3 3>&1)
#FILESYS=$(dialog --no-cancel --checklist 'title' 15 60 10 'ext4' '' 'on' 'xfs' '' 'off' 'btrfs' '' 'off' 3>&1 1>&2 2>&3 3>&1)
echo $FILESYS
echo $PASSDISK
echo $PASSDISKCONFIRM
echo $TYPEINSTALL
echo ${DISKLABEL[0]}
echo ${DISKLABEL[1]}
echo $CHOOSENDISK

